package org.hnau.logging.android.app

import android.app.Activity
import android.os.Bundle
import org.hnau.logging.base.logger

class AppActivity : Activity() {

    private val logger =
        logger<AppActivity>()

    override fun onCreate(savedInstanceState: Bundle?) {
        logger.warn("Test message", Exception("Test exception"))
        super.onCreate(savedInstanceState)
    }
}
