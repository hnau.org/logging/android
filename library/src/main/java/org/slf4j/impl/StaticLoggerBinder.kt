package org.slf4j.impl

import org.slf4j.spi.LoggerFactoryBinder


class StaticLoggerBinder : LoggerFactoryBinder {

    companion object {

        const val REQUESTED_API_VERSION = "1.6.99"

        private val loggerFactory =
            AndroidLoggerFactory()
        private val loggerFactoryClassStr = AndroidLoggerFactory::class.java.name

        private val singleton = StaticLoggerBinder()

        @JvmStatic
        public fun getSingleton() =
            singleton

    }

    override fun getLoggerFactory() =
        Companion.loggerFactory

    override fun getLoggerFactoryClassStr() =
        Companion.loggerFactoryClassStr


}