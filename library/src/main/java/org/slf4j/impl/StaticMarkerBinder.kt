package org.slf4j.impl

import org.slf4j.helpers.BasicMarkerFactory
import org.slf4j.spi.MarkerFactoryBinder


class StaticMarkerBinder: MarkerFactoryBinder {

    companion object {

        private val markerFactory = BasicMarkerFactory()
        private val markerFactoryClassStr = BasicMarkerFactory::class.java.name

        private val singleton = StaticMarkerBinder()

        @JvmStatic
        public fun getSingleton() =
            singleton

    }

    override fun getMarkerFactory() =
        Companion.markerFactory

    override fun getMarkerFactoryClassStr() =
        Companion.markerFactoryClassStr

}