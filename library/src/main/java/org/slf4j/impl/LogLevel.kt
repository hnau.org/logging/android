package org.slf4j.impl

import android.util.Log


enum class LogLevel(
    val key: Int
) {

    verbose(Log.VERBOSE),
    debug(Log.DEBUG),
    info(Log.INFO),
    warn(Log.WARN),
    error(Log.ERROR),
    assert(Log.ASSERT)

}