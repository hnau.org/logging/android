package org.slf4j.impl

import android.util.Log
import org.slf4j.helpers.MarkerIgnoringBase
import org.slf4j.helpers.MessageFormatter


class AndroidLogger(
        private val tag: String
) : MarkerIgnoringBase() {

    companion object {

        var logLevel: LogLevel? = LogLevel.warn

        private fun isLoggable(level: LogLevel) =
                logLevel?.let { it.key <= level.key } ?: false

    }

    private fun log(level: LogLevel, msg: String?, t: Throwable?) {
        if (!isLoggable(level)) return
        Log.println(
                level.key,
                tag,
                if (t == null) {
                    msg.toString()
                } else {
                    msg + '\n' + Log.getStackTraceString(t)
                }
        )
    }

    private fun log(level: LogLevel, msg: String?) =
            log(level, msg, null as Throwable?)

    private fun log(level: LogLevel, format: String?, arguments: Array<Any?>) {
        val formatted = MessageFormatter.format(format, arguments)
        log(level, formatted.message, formatted.throwable)
    }

    private fun log(level: LogLevel, format: String?, arg: Any?) =
            log(level, format, arrayOf(arg))

    private fun log(level: LogLevel, format: String?, arg1: Any?, arg2: Any?) =
            log(level, format, arrayOf(arg1, arg2))

    override fun warn(msg: String?) =
            log(LogLevel.warn, msg)

    override fun warn(format: String?, arg: Any?) =
            log(LogLevel.warn, format, arg)

    override fun warn(format: String?, vararg arguments: Any?) =
            log(LogLevel.warn, format, arguments)

    override fun warn(format: String?, arg1: Any?, arg2: Any?) =
            log(LogLevel.warn, format, arg1, arg2)

    override fun warn(msg: String?, t: Throwable?) =
            log(LogLevel.warn, msg, t)

    override fun info(msg: String?) =
            log(LogLevel.info, msg)

    override fun info(format: String?, arg: Any?) =
            log(LogLevel.info, format, arg)

    override fun info(format: String?, vararg arguments: Any?) =
            log(LogLevel.info, format, arguments)

    override fun info(format: String?, arg1: Any?, arg2: Any?) =
            log(LogLevel.info, format, arg1, arg2)

    override fun info(msg: String?, t: Throwable?) =
            log(LogLevel.info, msg, t)

    override fun debug(msg: String?) =
            log(LogLevel.debug, msg)

    override fun debug(format: String?, arg: Any?) =
            log(LogLevel.debug, format, arg)

    override fun debug(format: String?, vararg arguments: Any?) =
            log(LogLevel.debug, format, arguments)

    override fun debug(format: String?, arg1: Any?, arg2: Any?) =
            log(LogLevel.debug, format, arg1, arg2)

    override fun debug(msg: String?, t: Throwable?) =
            log(LogLevel.debug, msg, t)

    override fun error(msg: String?) =
            log(LogLevel.error, msg)

    override fun error(format: String?, arg: Any?) =
            log(LogLevel.error, format, arg)

    override fun error(format: String?, vararg arguments: Any?) =
            log(LogLevel.error, format, arguments)

    override fun error(format: String?, arg1: Any?, arg2: Any?) =
            log(LogLevel.error, format, arg1, arg2)

    override fun error(msg: String?, t: Throwable?) =
            log(LogLevel.error, msg, t)

    override fun trace(msg: String?) =
            log(LogLevel.verbose, msg)

    override fun trace(format: String?, arg: Any?) =
            log(LogLevel.verbose, format, arg)

    override fun trace(format: String?, vararg arguments: Any?) =
            log(LogLevel.verbose, format, arguments)

    override fun trace(format: String?, arg1: Any?, arg2: Any?) =
            log(LogLevel.verbose, format, arg1, arg2)

    override fun trace(msg: String?, t: Throwable?) =
            log(LogLevel.verbose, msg, t)

    override fun isErrorEnabled() =
            isLoggable(LogLevel.error)

    override fun isDebugEnabled() =
            isLoggable(LogLevel.debug)

    override fun isInfoEnabled() =
            isLoggable(LogLevel.info)

    override fun isWarnEnabled() =
            isLoggable(LogLevel.warn)

    override fun isTraceEnabled() =
            isLoggable(LogLevel.verbose)

}