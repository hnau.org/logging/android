package org.slf4j.impl

import org.slf4j.helpers.NOPMDCAdapter
import org.slf4j.spi.MDCAdapter


class StaticMDCBinder {

    companion object {

        private val mdcAdapter = NOPMDCAdapter()
        private val mdcAdapterClassStr = NOPMDCAdapter::class.java.name

        private val singleton = StaticMDCBinder()

        @JvmStatic
        public fun getSingleton() =
            singleton

    }

    fun getMDCA(): MDCAdapter =
        mdcAdapter

    fun getMDCAdapterClassStr() =
        mdcAdapterClassStr

}