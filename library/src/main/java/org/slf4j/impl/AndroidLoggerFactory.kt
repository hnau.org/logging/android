package org.slf4j.impl

import org.slf4j.ILoggerFactory


class AndroidLoggerFactory : ILoggerFactory {

    override fun getLogger(name: String) =
        AndroidLogger(name)

}